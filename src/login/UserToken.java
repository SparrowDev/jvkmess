package login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by a.gusev on 09.03.2017.
 */
public class UserToken {

    @SerializedName("Token")
    String UserToken;

    @SerializedName("UserId")
    Integer UserId;

    public UserToken(String Token, Integer Id){
        UserToken = Token;
        UserId = Id;
    }

    public String getUserToken(){
        return UserToken;
    }

    public Integer getUserId() {
        return UserId;
    }
}
