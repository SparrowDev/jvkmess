package login;/**
 * Created by a.gusev on 01.03.2017.
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.sun.javafx.fxml.builder.URLBuilder;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.messages.Message;
import com.vk.api.sdk.objects.messages.responses.GetResponse;
import com.vk.api.sdk.objects.users.User;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import main.Main;
import sun.misc.Regexp;

import java.awt.*;
import java.io.*;
import java.net.InterfaceAddress;
import java.net.URL;
import java.util.Scanner;

public class Login extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        TransportClient transportClient = HttpTransportClient.getInstance();
        VkApiClient vk = new VkApiClient(transportClient);

        WebView webView = new WebView();
        Text text = new Text();
        text.minHeight(50);
        text.minWidth(650);

        BorderPane borderPane = new BorderPane();

        webView.getEngine().locationProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(newValue.matches(".*blank.html#code.*")){
                   //primaryStage.hide();
                    String code = newValue.split("=")[1];

                    UserAuthResponse authResponse = null;
                    try {
                        authResponse = vk.oauth()
                                .userAuthorizationCodeFlow(5899855, "tQ5jV9vVS1971dJKdksX", "https://oauth.vk.com/blank.html", code)
                                .execute();
                    } catch (ApiException e) {
                        e.printStackTrace();
                    } catch (ClientException e) {
                        e.printStackTrace();
                    }

                    UserActor actor = new UserActor(authResponse.getUserId(), authResponse.getAccessToken());

                    try {
                        SaveToken(new UserToken(actor.getAccessToken(), actor.getId()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    try {
                        GetResponse message = vk.messages().get(actor).execute();

                        java.util.List mess = message.getItems();

                        mess.size();
                    } catch (ApiException e) {
                        e.printStackTrace();
                    } catch (ClientException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    text.setText(newValue);
                }
            }
        });
        int accesScope = 65536 | 4096;
        webView.getEngine().load("https://oauth.vk.com/authorize?client_id=5899855&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=" + accesScope + "&response_type=code&v=5.62");
       // UserActor actor = new UserActor(112023596, "e0bece1b3aebbc6f7fbd9e48aff96cc9d4ee6e2153cc4aeb0807f7f7503cd068ffef5a38929fcf4aeacd6");

        borderPane.setTop(text);
        borderPane.setCenter(webView);

        Scene scene = new Scene(borderPane,950,500, Color.web("#666970"));
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void SaveToken(UserToken token) throws IOException {
        Gson gson = new Gson();

        String json = gson.toJson(token);

        FileWriter fileWriter = new FileWriter("UserToken");

        fileWriter.write(json);
        fileWriter.close();
    }

    public static UserToken GetUserToken() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("UserToken"));

        Gson gson = new Gson();

        return gson.fromJson(scanner.next(), UserToken.class);
    }
}
