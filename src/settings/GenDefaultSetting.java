package settings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by a.gusev on 28.02.2017.
 */
public class GenDefaultSetting {

    static void GenerateDefaultSetting() throws IOException {
        Settings DefSettings = new Settings();
        DefSettings.IsHideStart = false;
        DefSettings.updateTime = 15;

        Gson gson = new Gson();
        String json = gson.toJson(DefSettings, Settings.class);

        FileWriter fileWriter = new FileWriter(Settings.DefaultSettingPath);
        fileWriter.write(json);
        fileWriter.flush();
        fileWriter.close();
    }

}
