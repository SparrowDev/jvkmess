package settings;

import com.google.gson.Gson;

import java.io.*;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by a.gusev on 28.02.2017.
 */
public class LoaderSetting {


    public static Settings GetSetting() throws IOException {
        Settings resultSetting = null;
        File SettingFile = new File(Settings.DefaultSettingPath);

        try {
            Scanner scanner = new Scanner(SettingFile);
            Gson gson = new Gson();
            resultSetting = gson.fromJson(scanner.next(), Settings.class);
        } catch (Exception e) {
            GenDefaultSetting.GenerateDefaultSetting();
            resultSetting = GetSetting();
        }
        
        return resultSetting;
    }
}
