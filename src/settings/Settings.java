package settings;

import com.google.gson.annotations.SerializedName;
import main.Main;

/**
 * Created by a.gusev on 28.02.2017.
 */
public class Settings {

    /**
     * Путь к настройкам по умолчанию
     */
    public static final String DefaultSettingPath = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath() + "settings.json";

    /**
     * Сворачивание приложения в трей при старте
     */
    @SerializedName("HideStart")
    boolean IsHideStart;

    /**
     * Интервал обновления в секундах
     */
    @SerializedName("UpdateTime")
    int updateTime;

    @SerializedName("positionMessageX")
    int positionX;

    @SerializedName("positionMessageY")
    int positionY;

    public void SetIsHideStart(boolean hide){
        IsHideStart = hide;
    }

    public boolean GetIsHideStart(){
        return IsHideStart;
    }

    public int getUpdateTime() {
        return updateTime;
    }
}
