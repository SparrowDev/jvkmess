package main;/**
 * Created by a.gusev on 09.03.2017.
 */

import com.google.gson.Gson;
import com.sun.javafx.fxml.builder.JavaFXSceneBuilder;
import com.sun.javafx.tk.Toolkit;
import com.vk.api.sdk.client.ApiRequest;
import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.UserAuthResponse;
import com.vk.api.sdk.objects.fave.responses.GetUsersResponse;
import com.vk.api.sdk.objects.messages.Chat;
import com.vk.api.sdk.objects.messages.Dialog;
import com.vk.api.sdk.objects.messages.Message;
import com.vk.api.sdk.objects.messages.responses.GetChatUsersChatIdsFieldsResponse;
import com.vk.api.sdk.objects.messages.responses.GetDialogsResponse;
import com.vk.api.sdk.objects.messages.responses.GetResponse;
import com.vk.api.sdk.objects.users.User;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.messages.MessagesGetChatQuery;
import com.vk.api.sdk.queries.messages.MessagesGetChatUsersQuery;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import login.Login;
import login.UserToken;
import settings.LoaderSetting;
import settings.Settings;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import Jobs.*;
import vkAPI.Api;

import javax.swing.table.TableStringConverter;

public class MainApplication extends Application {

    public static Settings setting = null;

    public static ArrayList<Jobs> jobs = new ArrayList<Jobs>();

    public static TextArea textArea;

    public static Text text;

    public static String string = "";

    public static void main(String[] args) {
        launch(args);
    }

    public static UserToken token;

    public static Stage globalStage;

    public static int lastId;

    public static int lastX = 1700;

    public static int lastY = 100;

    @Override
    public void init() throws Exception {
        super.init();

        initSetting();

        initClient();

        initJobs();
    }

    public void initSetting() throws IOException {
        setting = LoaderSetting.GetSetting();

        try {
            token = Login.GetUserToken();
        } catch (FileNotFoundException e) {
            token = null;
        }

        if (token != null) {
            Api.actor = new UserActor(token.getUserId(), token.getUserToken());
        }

        try {
            lastId = MessageHandler.GetLastMessage();
        }
        catch (FileNotFoundException e){
            lastId = 0;
        }
    }

    public void initClient(){
        TransportClient transportClient = HttpTransportClient.getInstance();

        Api.vk = new VkApiClient(transportClient, new Gson());
    }

    public void initJobs() throws IOException {
        jobs.add(new MessageJob());
    }

    public void run(){

        while (true){
            for(Jobs job : jobs){
                try {
                    job.doJob();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                TimeUnit.SECONDS.sleep(setting.getUpdateTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Scene scene = null;
        String Caption = "";
        globalStage = new Stage();
        text = new Text();

        if (token == null){
            WebView webView = new WebView();
            text.minHeight(50);
            text.minWidth(650);

            BorderPane borderPane = new BorderPane();
            int accesScope = 65536 | 4096;
            webView.getEngine().load("https://oauth.vk.com/authorize?client_id=5899855&display=page&redirect_uri=https://oauth.vk.com/blank.html&scope=" + accesScope + "&response_type=code&v=5.62");
            borderPane.setTop(text);
            borderPane.setCenter(webView);
            scene = new Scene(borderPane,950,500, Color.web("#666970"));

            webView.getEngine().locationProperty().addListener(changeListener);
        }
        else{
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        GetResponse message = Api.vk.messages().get(Api.actor).out(false).lastMessageId(lastId).count(5).execute();
                        HandlerMessage(message);
                    } catch (ApiException e) {
                        e.printStackTrace();
                    } catch (ClientException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.setDaemon(true);
            thread.start();
        }
    }

    public void HandlerMessage(GetResponse message){
        if (!message.getItems().isEmpty()) {
            for (Message mess : message.getItems()) {
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        MessageHandler.HandlerMessage(mess);
                    }
                });

                lastId = mess.getId();
            }
            try {
                MessageHandler.WriteLastMessage(lastId);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ChangeListener changeListener = new ChangeListener<String>() {
        @Override
        public void changed(ObservableValue observable, String oldValue, String newValue) {
            if(newValue.matches(".*blank.html#code.*")){
                //primaryStage.hide();
                String code = newValue.split("=")[1];

                UserAuthResponse authResponse = null;
                try {
                    authResponse = Api.vk.oauth()
                            .userAuthorizationCodeFlow(5899855, "tQ5jV9vVS1971dJKdksX", "https://oauth.vk.com/blank.html", code)
                            .execute();
                } catch (ApiException e) {
                    e.printStackTrace();
                } catch (ClientException e) {
                    e.printStackTrace();
                }

                Api.actor = new UserActor(authResponse.getUserId(), authResponse.getAccessToken());

                try {
                    Login.SaveToken(new UserToken(Api.actor.getAccessToken(), Api.actor.getId()));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Task task = new Task() {
                    @Override
                    protected Object call() throws Exception {
                        Stage stage = new Stage();

                       // stage.setScene(GetMessageScene());
                        stage.setTitle("Message");
                        globalStage.hide();
                        stage.show();
                        return stage;
                    }
                };

                task.run();
            }
            else{
                text.setText(newValue);
            }
        }
    };

    private Scene GetMessageScene(String message){
        BorderPane borderPane = new BorderPane();
        borderPane.setCenter(text);
        text.textProperty().setValue(message);

        Scene scene = new Scene(borderPane,200,100, Color.web("#AA666970"));
        return scene;
    }


}
