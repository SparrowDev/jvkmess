package vkAPI;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.users.UserXtrCounters;

/**
 * Created by a.gusev on 22.03.2017.
 */
public class Api {

    public static UserActor actor = null;

    public static VkApiClient vk;

    public static String GetUserName(String userId)  {
        UserXtrCounters user = null;
        try {
            user = Api.vk.users().get(Api.actor).userIds(userId).execute().get(0);
        } catch (ApiException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return user.getLastName() + " " + user.getFirstName();
    }
}
