package Jobs;

import com.google.gson.Gson;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import javafx.animation.PauseTransition;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import login.UserToken;
import main.MainApplication;
import vkAPI.Api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by a.gusev on 22.03.2017.
 */
public class MessageHandler {

    private static final String NameLastMessageFile = "LastMessage";

    public static void WriteLastMessage(int idLast) throws IOException {
        Gson gson = new Gson();

        String json = gson.toJson(idLast);

        FileWriter fileWriter = new FileWriter(NameLastMessageFile);

        fileWriter.write(json);
        fileWriter.close();
    }

    public static Integer GetLastMessage() throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(NameLastMessageFile));

        Gson gson = new Gson();

        return gson.fromJson(scanner.next(), Integer.class);
    }

    public static void HandlerMessage(Message message){
        Stage stage = new Stage(StageStyle.TRANSPARENT);
        BorderPane borderPane = new BorderPane();

        Text messText = new Text();
        Text headText = new Text();

        borderPane.setCenter(messText);
        borderPane.setTop(headText);

        messText.setText(message.getBody());
        messText.setWrappingWidth(200);

        headText.setText(Api.GetUserName(message.getUserId().toString()));

        stage.setX(MainApplication.lastX);
        stage.setY(MainApplication.lastY);

        MainApplication.lastY += 110;

        stage.setScene(new Scene(borderPane, 200, 100));
        stage.show();

        PauseTransition pauseTransition = new PauseTransition(Duration.seconds(5));
        pauseTransition.setOnFinished(event -> {stage.close();});
        pauseTransition.play();
    }
}
