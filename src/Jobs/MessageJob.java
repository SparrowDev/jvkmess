package Jobs;

import com.vk.api.sdk.objects.messages.Message;
import com.vk.api.sdk.objects.messages.responses.GetResponse;
import javafx.beans.binding.Bindings;
import main.Main;
import main.MainApplication;

/**
 * Created by a.gusev on 10.03.2017.
 */
public class MessageJob implements Jobs {


    @Override
    public void doJob() throws Exception {
        GetResponse response = MainApplication.vk
                                              .messages()
                                              .get(MainApplication.actor)
                                              .out(false)
                                              .count(10)
                                              .execute();
        String str = null;
        for(Message message : response.getItems()){
            str += message.getBody();
        }

        MainApplication.string = str;
    }
}
